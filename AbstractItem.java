package pizzashop;

import pizzashop.food.Size;
/**
 * Abstract class collect same thing that Pizza and Drinks have in common.
 * @author Chinatip Vichian
 *
 */
public abstract class AbstractItem implements OrderItem{
	/** Number that helps find size when call getSize method */
	private int size;
	
	/** Constructor of AbstractItem class */
	public AbstractItem(int size) {
		this.size = size;
	}
	
	/**
	 * Get Size enum
	 * @return Size enum called by Pizza and Drink class
	 */
	public Size getSize(){
		Size sizes = Size.NONE;
		sizes = sizes.getSize(size);
		return sizes;
	}
}
