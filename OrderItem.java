package pizzashop;
/**
 * Interface for Pizza and Drink class that was used in FoodOrder to get them within one method.
 * @author Chinatip Vichian
 *
 */
public interface OrderItem {
	/** Get price of a product */
	public double getPrice();
	/** Get String of a product */
	public String toString();
}
