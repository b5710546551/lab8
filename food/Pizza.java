package pizzashop.food;
import java.util.*;

import pizzashop.*;
/** 
 * A pizza, of course.
 * @author Chinatip Vichian
 */
public class Pizza extends AbstractItem implements OrderItem{
	// constants related to drink size
	public static final double [] prices = { 0.0, 120.0, 200.0, 300.0 };
	/** list of toppings on this pizza */
	private List<Topping> toppings;
	
	/** A new pizza.
	 * @param size is 1 for small, 2 for medium, 3 for large.
	 */
	public Pizza( int size ) {
		super(size);
		toppings = new ArrayList<Topping>();
	}

	/* String description of this pizza */
	public String toString() {
		StringBuffer tops = new StringBuffer();
		if ( toppings.size() == 0 || 
				(toppings.size()==1 && toppings.get(0) == Topping.NONE ) ) 
			/* don't print anything */;
		else {
			tops.append(" with ");
			tops.append(toppings.get(0).toString());
			for( int k = 1; k<toppings.size(); k++ ) {
				tops.append(", ");
				tops.append( toppings.get(k).toString() );
			}
		}
	
		return String.format("%s pizza%s", super.getSize().name, tops.toString() );
	}

	/* @see pizzashop.FoodItem#getPrice() */
	public double getPrice() {
		double price = 0;
		if (super.getSize().num != 0 ) price =  prices[super.getSize().num];
		return price;
	}

	/** 
	 * add a topping
	 * @param topping is a topping to add to pizza
	 */
	public void add(Topping topping) {
		toppings.add( topping );
	}
	/**
	 * Remove toppings from toppings list
	 * @param topping is Topping that wanted to be removed
	 */
	public void remove(Topping topping) {
		toppings.remove(topping);
	}
	/**
	 * Get list of toppings
	 * @return List of toppings
	 */
	public List<Topping> getToppings( ) {
		return toppings;
	}
	/**
	 * Set list of toppings to be the new one coming from parameter
	 * @param toppings is a new toppings that come to set the old list
	 */
	public void setToppings( List<Topping> toppings ) {
		this.toppings = toppings;
	}
	/**
	 * Clone the object
	 */
	public Object clone() {
		Pizza clone = null;
		try {
			clone = (Pizza) super.clone();
			clone.getSize().num = super.getSize().num;
			// copy toppings into a new List in clone
			clone.toppings = new ArrayList<Topping>();
			for(Topping t : toppings ) clone.toppings.add(t);
		} catch (CloneNotSupportedException e) {
			System.out.println("Pizza.clone: " + e.getMessage() );
		}
		
		return clone;
	}

}
