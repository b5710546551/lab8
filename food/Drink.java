package pizzashop.food;

import pizzashop.AbstractItem;
import pizzashop.OrderItem;

/** 
 * A drink has a size and flavor.
 * @author Chinatip Vichian
 */
public class Drink extends AbstractItem implements OrderItem{
	// constants related to drink size
	public static final double [] prices = { 0.0, 20.0, 30.0, 40.0 };
	/** String of flavor of the drink */
	private String flavor;

	/**
	 * create a new drink.
	 * @param size is the size. 1=small, 2=medium, 3=large
	 */
	public Drink( int size ) {
		super(size);
	}

	/* 
	 * Get String of this class
	 * @return String of detail of drink
	 */
	public String toString() {
		return super.getSize().name + " " + (flavor==null? "": flavor.toString()) + " drink"; 
	}

	/** return the price of a drink
	 * @see pizzashop.FoodItem#getPrice()
	 */
	public double getPrice() {
		if (super.getSize().num != 0 ) return prices[super.getSize().num];
		return 0.0;
	}
	/**
	 * Clone the object
	 */
	public Object clone() {
		Drink clone;

		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		//superclass already cloned the size and flavor
		return clone;
	}
}
