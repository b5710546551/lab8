package pizzashop.food;
/**
 * Size enum collect size and its size number 
 * @author Chinatip Vichian
 *
 */
public enum Size {
	NONE("None",0),
	SMALL("Small",1),
	MEDIUM("Medium",2),
	LARGE("Large",3);
	/** String of name of the size */
	public String name;  
	/** size number that will helps to find prices */
	public int num;
	/** Constructor of Size Enum */
	Size(String name, int num){
		this.name = name;
		this.num = num;
	}
	/**
	 * Get Size Enum by put int of number that tell size in the parameter
	 * @param num is number of size
	 * @return Size Enum that match to the size number from parameter
	 */
	public Size getSize(int num){
		int i =0;
		Size[] sizeEnum = values();
		while (sizeEnum[i].num == num && i < values().length){
			i++;
		}
		return sizeEnum[i];
	}
	
	public int getNum(){ return this.num; }
	public String getName(){ return this.name; }
	
}
